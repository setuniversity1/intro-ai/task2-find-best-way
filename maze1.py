import sys
import math

class Node():
    def __init__(self, state, parent, action, cost):
        self.state = state
        self.parent = parent
        self.action = action
        self.cost = cost
    
    def total_cost(self):
        # Implement the calculation for total cost (g(n) + h(n)) here
        # Placeholder - replace this with your actual calculation
        return self.cost

class Maze():
    def __init__(self, filename):
        with open(filename) as f:
            contents = f.read()

        if contents.count("A") != 1:
            raise Exception("maze must have exactly one start point")
        if contents.count("B") != 1:
            raise Exception("maze must have exactly one goal")

        contents = contents.splitlines()
        self.height = len(contents)
        self.width = max(len(line) for line in contents)

        self.walls = []
        for i in range(self.height):
            row = []
            for j in range(self.width):
                try:
                    if contents[i][j] == "A":
                        self.start = (i, j)
                        row.append(False)
                    elif contents[i][j] == "B":
                        self.goal = (i, j)
                        row.append(False)
                    elif contents[i][j] == " ":
                        row.append(False)
                    else:
                        row.append(True)
                except IndexError:
                    row.append(False)
            self.walls.append(row)

        self.solution = None

    def print(self):
        solution = self.solution[1] if self.solution is not None else None
        print()
        for i, row in enumerate(self.walls):
            for j, col in enumerate(row):
                if col:
                    print("█", end="")
                elif (i, j) == self.start:
                    print("A", end="")
                elif (i, j) == self.goal:
                    print("B", end="")
                elif solution is not None and (i, j) in solution:
                    print("*", end="")
                else:
                    print(" ", end="")
            print()
        print()

    def neighbors(self, state):
        row, col = state
        candidates = [
            ("up", (row - 1, col)),
            ("down", (row + 1, col)),
            ("left", (row, col - 1)),
            ("right", (row, col + 1))
        ]

        result = []
        for action, (r, c) in candidates:
            if 0 <= r < self.height and 0 <= c < self.width and not self.walls[r][c]:
                result.append((action, (r, c)))
        return result

    def solve(self):
        start = Node(state=self.start, parent=None, action=None, cost=0)
        frontier = [start]

        while frontier:
            frontier.sort(key=lambda x: x.total_cost())  # Sort based on total cost

            current_node = frontier.pop(0)  # Get the node with the lowest total cost
            
            if current_node.state == self.goal:
                actions = []
                cells = []
                while current_node.parent is not None:
                    actions.append(current_node.action)
                    cells.append(current_node.state)
                    current_node = current_node.parent
                actions.reverse()
                cells.reverse()
                self.solution = (actions, cells)
                return

            for action, state in self.neighbors(current_node.state):
                if state not in [node.state for node in frontier]:
                    # Calculate cost for the new node
                    new_cost = current_node.cost + 1  # Assuming uniform cost

                    # Create the new node
                    new_node = Node(state, current_node, action, new_cost)

                    # Add the new node to the frontier
                    frontier.append(new_node)

    def output_image(self, filename, show_solution=True, show_explored=False):
        # Add image generation logic if required
        pass

# Implement your heuristic calculation function
def heuristic_cost_estimate(current_state, goal_state):
    # Placeholder heuristic - replace this with your actual heuristic calculation
    x1, y1 = current_state
    x2, y2 = goal_state
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("Usage: python maze.py maze.txt")

    m = Maze(sys.argv[1])
    print("Maze:")
    m.print()
    print("Solving...")
    m.solve()
    print("States Explored:", m.num_explored)
    print("Solution:")
    m.print()
